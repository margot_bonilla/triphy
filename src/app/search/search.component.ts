import { Component } from '@angular/core';
import { AppState } from '../app.service';
import {SearchService} from "./search.service";
import {StartDanceDirective} from "../directive/start.dance.directive";

@Component({
    selector: 'search',
    providers: [SearchService],
    styles: [ require('!raw!sass!./search.scss') ],
    template: require('./search.html'),
    directives: [StartDanceDirective]
})
export class Search {

    tags = '';
    images = [];

    // TypeScript public modifiers
    constructor(public searchService: SearchService ) {

    }

    search() {
        console.log('searching . . .', this.tags.split(','));
        this.images = this.searchService.get(this.tags);
    }
    dynamic(element) {
        console.log(element);
    }
    clear(event) {
        this.tags = '';
        this.images = [];
    }

}
