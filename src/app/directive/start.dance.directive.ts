import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[startDance]',
    host: {
        '(mouseenter)': 'onMouseEnter()',
        '(mouseleave)': 'onMouseLeave()'
    }
})
export class StartDanceDirective {
    private el:HTMLElement;
    private _imageUrl: "";
    constructor(el: ElementRef) { this.el = el.nativeElement; }

    @Input() set imageUrl(imageUrl:string){
        this._imageUrl = imageUrl;
    }
    @Input('startDance') danceImage: string;

    onMouseEnter() { this.activeImage(this.danceImage || this._imageUrl); }
    onMouseLeave() { this.activeImage(null); }

    private activeImage(imageUrl: string) {
        this.el.src = imageUrl || this._imageUrl;
    }

}