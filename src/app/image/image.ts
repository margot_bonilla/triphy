export class Image {
    id: number;
    staticUrl: string;
    url: string;
    tags: string[];
    createdBy: number;
    width: number;
    height: number;
    visualizations: number;
}