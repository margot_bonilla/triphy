import {Image} from "./image";

export const IMAGES: Image[] = [
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    },
    {
        id: 1,
        staticUrl: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w.gif",
        url: "https://media1.giphy.com/media/3o6gb2JPvixhZyevYI/200w_s.gif",
        tags: ["wololo", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 2,
        staticUrl: "https://media4.giphy.com/media/pcC2u7rl89b44/200.gif",
        url: "https://media4.giphy.com/media/pcC2u7rl89b44/200_s.gif",
        tags: ["cat", "moein"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 2
    },
    {
        id: 3,
        url: "https://media-mediatemple.netdna-ssl.com/wp-content/uploads/2015/06/10-dithering-opt.jpg",
        staticUrl: null,
        tags: ["beer", "crazy"],
        createdBy: 0,
        width: 100,
        height: 100,
        visualizations: 7
    }
];